<?php

namespace App\Repository;

use App\Entity\Article;

class ArticleRepository
{

    private $connection;

    
    public function __construct()
    {
        try {

            $this->connection = new \PDO(
                "mysql:host=" . getenv("MYSQL_HOST") . ":3306;dbname=" . getenv("MYSQL_DATABASE"),
                getenv("MYSQL_USER"),
                getenv("MYSQL_PASSWORD")
            );

            $this->connection->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);

        } catch (\PDOException $e) {

            dump($e);

        }
    }
    /**
     * @param string 
     * @param array|null 
     * @return array|User|null 
     */
    private function fetch(string $query, array $params = []){
        try {

            $query = $this->connection->prepare($query);
            
            foreach ($params as $param => $value) {
                $query->bindValue($param, $value);
            }
          
            $query->execute();

            $result = [];
          
            foreach ($query->fetchAll() as $row) {
                $result[] = User::fromSQL($row);
            }


            if (count($result) <= 1) {
                return $result[0];
            }
            return $result;

        } catch (\PDOException $e) {
            dump($e);
        }

    }
    /**
     * La méthode `add()` permet d'insérer un nouvel utilisateur dans la base de données, elle retourne l'utilisateur fraîchement créé.
     * 
     * @param User $user L'utilisateur à insérer dans la base de données.
     * @return User L'utilisateur fraîchement créé.
     */
    public function add(User $user)
    {
        // on appelle la méthode privée `fetch()` que l'on à créée ci-dessus pour inserer notre utilisateur.
        $this->fetch("INSERT INTO user (name, surname, email, birthdate, gender) VALUES (:name, :surname, :email, :birthdate, :gender)",[
            ":name" => $user->name,
            ":surname" => $user->surname,
            ":email" => $user->email,
            ":birthdate" => $user->birthdate,
            ":gender" => $user->gender
        ]);
        // lorsque l'utilisateur est créé dans base de données,
        // on affecte la valeur de l'id qui vient d'être inséré à l'instance de `User` ...
        $user->id = intval($this->connection->lastInsertId());
        // ... puis on retourne l'utilisateur
        return $user;
    }

    /**
     * La méthode `get()` permet de sélectionner un utilisateur dans la base de données à partir d'un id.
     * 
     * @param int $id L'id de l'utilisateur à selectionner.
     * @return User|null L'utilisateur correspondant à l'id.
     */
    public function get(int $id)
    {
        // on appelle la méthode privée `fetch()` pour selectionner notre utilisateur.
        return $this->fetch("SELECT * FROM user WHERE :id", [":id" => $id]);
    }

    /**
     * La méthode `getAll()` permet de sélectionner tout les utilisateurs dans la basede données.
     * 
     * @param int $id L'id de l'utilisateur à selectionner.
     * @return array|User|null L'utilisateur correspondant à l'id.
     */
    public function getAll()
    {
        // on appelle la méthode privée `fetch()` pour selectionner tout les utilisateurs.
        return $this->fetch("SELECT * FROM user");
    }

}
