<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use App\Entity\Article;



class ArticleType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
       
        $builder
            ->add('id', IntegrerType::class)
            ->add('title', TextType::class)
            ->add('author', TextType::class)
            ->add('type',TextType::class);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
       
        $resolver->setDefaults([
            "data_class" => Article::class
        ]);
    }
}
