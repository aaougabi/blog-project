<?php


namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Repository\ArticleRepository;
use App\Form\ArticleType;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
* @Security("has_role('ROLE_ADMIN')")
*/
class AmrController extends AbstractController
{
    /**
     * @Route("/article/{id}", name="article")
     */
    public function index(int $id , ArticleRepository $repo, Request $request)
    {
        $article = $repo->getById($id);

        $form = $this->createForm(ArticleType::class, $article);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $repo->update($form->getData());
            return $this->redirectToRoute("home");
        }

        return $this->render('article/add_article.html.twig', [
            "form" => $form->createView(),
            "article" => $article
        ]);
    }

    /**
     * @Route("/article/remove/{id}", name="remove_article")
     */
    public function remove(int $id, ArticleRepository $repo) {
        $repo->delete($id);
        return $this->redirectToRoute("home");
    }
}
