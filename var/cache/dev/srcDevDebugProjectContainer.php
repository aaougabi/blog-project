<?php

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.

if (\class_exists(\Container04lgN5X\srcDevDebugProjectContainer::class, false)) {
    // no-op
} elseif (!include __DIR__.'/Container04lgN5X/srcDevDebugProjectContainer.php') {
    touch(__DIR__.'/Container04lgN5X.legacy');

    return;
}

if (!\class_exists(srcDevDebugProjectContainer::class, false)) {
    \class_alias(\Container04lgN5X\srcDevDebugProjectContainer::class, srcDevDebugProjectContainer::class, false);
}

return new \Container04lgN5X\srcDevDebugProjectContainer(array(
    'container.build_hash' => '04lgN5X',
    'container.build_id' => '9ecd2168',
    'container.build_time' => 1531318851,
), __DIR__.\DIRECTORY_SEPARATOR.'Container04lgN5X');
