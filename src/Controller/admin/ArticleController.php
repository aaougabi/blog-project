<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Entity\SmallDog;
use Symfony\Component\HttpFoundation\Request;
use App\Repository\ArticleRepository;
use App\Form\ArticleType;

class ArticleController extends AbstractController
{
    /**
     * @Route("/src/article", name="article")
     */
    public function index(Request $request, ArticleRepository $repo)
    {
        
        $form = $this->createForm(ArticleType::class);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
           
            $repo->add($form->getData());
           
            return $this->redirectToRoute("home");
        }


        return $this->render('article/add_article.html.twig', [
            'form' => $form->createView()
        ]);
    }
}
